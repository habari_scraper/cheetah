#!/bin/bash

cd /home/ubuntu/habari
#scrapy allcrawl
# MongoDB is running on mongoHQ
# if ps aux | grep "[m]ongod"
# then
#     echo "MongoDB is running" >> log.txt
# else
#     sudo mongod
#     echo "MongoDB has been restarted" >> log.txt
# fi

if ps aux | grep "scrapyd"
then
    echo "Scrapyd is running" >> log.txt
else
    sudo scrapyd
    echo  "Scrapyd has been restarted" >> log.txt
fi

echo "Starting daily scraper"

scrapy allcrawl

echo "Daily Scraper Successful: $(date)" >> log.txt
echo "\n\n\n\n" >> log.txt
