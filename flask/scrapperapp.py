from flask import Flask, jsonify, make_response, request, Response
from pymongo import MongoClient
import json, re, time
from bson import json_util

app = Flask('habari')
# add this so that flask doesn't swallow error messages
app.config['PROPAGATE_EXCEPTIONS'] = True

#mongo = PyMongo(app)
MONGOHQ_URL = "mongodb://admin:pass@kahana.mongohq.com/habari"
client = MongoClient(MONGOHQ_URL, 10084)
db = client['habari']
collection = db['popular']

#a base urls that returns all the parks in the collection (of course in the future we would implement paging)
@app.route("/")
@app.route("/popular")
def popular():
    #setup the connection

    #retrieve the count argument from request
    count = request.args.get('count', type=int)
    if count is None:
        count = 20
    #query the DB for the latest news articles
    results = collection.find({}).limit(count).sort('UTC', -1)

    #Now turn the results into valid JSON
    json_results = []
    amazons3_domain = "http://s3.amazonaws.com/habari-dev2014/"
    for result in results:
        if len(result['images']):
            image_path = result['images'][0]['path']
            assert isinstance(image_path.replace, object)

            result.update(dict(image_large=amazons3_domain + image_path,
                               image_thumb=amazons3_domain + image_path.replace("full", "thumbs/large"),
                               image_small=amazons3_domain + image_path.replace("full", "thumbs/small"),
                               image_medium=amazons3_domain + image_path.replace("full", "thumbs/medium")))



        obj_id = str(result['_id'])
        a = ''.join(re.findall(r'\d+', obj_id))

        result.update(dict(original_image_width=595,
                           original_image_height=300,
                           source='Nation Media Group',
                           id=int(a)))

        del result['_id']
        del result['tag']
        del result['UTC']
        del result['images']
        del result['image_urls']

        json_results.append(result)

    js = json.dumps(list(json_results), sort_keys=True, indent=4, default=json_util.default)

    response = Response(js, status=200, mimetype='application/json')
    return response


@app.route("/popular/<category>")
@app.route("/popular/<category>/<int:count>")
def popularWithTag(category, count=20):
    results = collection.find({'tag': {"$in": [category]}}).limit(count).sort('UTC', -1)
    #Now turn the results into valid JSON
    json_results = []
    amazons3_domain = "http://s3.amazonaws.com/habari-dev2014/"
    for result in results:
        if len(result['images']):
            image_path = result['images'][0]['path']
            assert isinstance(image_path.replace, object)

            result.update(dict(image_large=amazons3_domain + image_path,
                               image_thumb=amazons3_domain + image_path.replace("full", "thumbs/large"),
                               image_small=amazons3_domain + image_path.replace("full", "thumbs/small"),
                               image_medium=amazons3_domain + image_path.replace("full", "thumbs/medium")))



        obj_id = str(result['_id'])
        a = ''.join(re.findall(r'\d+', obj_id))

        result.update(dict(original_image_width=595,
                           original_image_height=300,
                           source='Nation Media Group',
                           id=int(a)))

        del result['_id']
        del result['tag']
        del result['UTC']
        del result['images']
        del result['image_urls']

        json_results.append(result)

    js = json.dumps(list(json_results), sort_keys=True, indent=4, default=json_util.default)

    response = Response(js, status=200, mimetype='application/json')
    return response


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'Error': 'Something bad happen, but we still love you'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'Error': 'You just got 404d dude!'}), 404)


@app.route("/test")
def test():
    results1 = request.args.get('count')
    return 'with count %d' + results1


#need this in a scalable app so that HAProxy thinks the app is up
@app.route("/")
def welcome():
    return make_response(jsonify({'Hello': 'Welcome to habari server'}), 404)


if __name__ == "__main__":
    app.run(debug=True)
