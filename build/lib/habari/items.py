# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class HabariItem(Item):
    title = Field()
    uri = Field()
    category = Field()
    summary = Field()
    content = Field()
    author = Field()
    published = Field()
    image_urls = Field()
    images = Field()
    caption = Field()
    tag = Field()
    publisher = Field()
    UTC = Field()
    category = Field()
