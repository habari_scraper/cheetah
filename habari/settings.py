# Scrapy settings for habari project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
import sys
import os
BOT_NAME = 'habari'
COMMANDS_MODULE = 'habari.commands'
SPIDER_MODULES = ['habari.spiders']
NEWSPIDER_MODULE = 'habari.spiders'

ITEM_PIPELINES = {'habari.pipelines.MongoDBPipeline':300, 'scrapy.contrib.pipeline.images.ImagesPipeline':1}
SPIDER_MIDDLEWARES = {'habari.middleware.deltafetch.DeltaFetch': 100,}
DELTAFETCH_ENABLED = True
DOTSCRAPY_ENABLED = True

MONGODB_SERVER = "mongodb://admin:pass@kahana.mongohq.com/habari"
MONGODB_PORT = 10084
MONGODB_DB = "habari"
MONGODB_COLLECTION = "popular"

AWS_ACCESS_KEY_ID = 'AKIAIGXRVKGMVQFJP2EQ'
AWS_SECRET_ACCESS_KEY = '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'
IMAGES_STORE = 's3://habari-dev2014/'
IMAGE_EXPIRES = 90
IMAGES_THUMBS = {
    'small': (100, 100),
    'medium':(250, 250),
    'large':(320, 180)
}


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'habari (+http://www.yourdomain.com)'
