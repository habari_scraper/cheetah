from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from habari.items import HabariItem
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
import urlparse
import time


class MainParser():
    def __init__(self, response, tags, category):
        self.response = response
        self.tags = tags
        self.category = category

    def scrapNationOnline(self):
        hxs = Selector(self.response)
        item = HabariItem()
        item["tag"] = self.tags
        item["uri"] = self.response.url
        published = ''.join(hxs.xpath('//div[@class=\"story-view\"]/header/h5/text()').extract())
        published = published.replace(u'\xa0', u' ')
        timeStamp = time.strptime(published, " %A, %B %d, %Y");
        item["UTC"] = time.mktime(timeStamp)
        month = str(timeStamp.tm_mon).zfill(2)
        item['published'] = str(('%s-%s-%s' % (timeStamp.tm_year, month, timeStamp.tm_mday)))
        item["title"] = ''.join(hxs.xpath(
            '//div[@class=\"story-view\"]/header/h1/text()').extract())  # this worked by taking the page title, contains tags we dont need/want ''.join(hxs.css('title::text').extract())
        item["summary"] = ''.join(hxs.xpath('//div/ul/li/text()').extract())
        item["content"] = '\n'.join(hxs.xpath('//article/section/div/p/text()').extract())
        item["caption"] = ''.join(hxs.xpath('//img[@class=\"photo_article\"]/@alt').extract())

        # retrieve an image, some images have a relative url
        relative_url = ''.join(hxs.xpath('//img[@class=\"photo_article\"]/@src').extract())
        item["image_urls"] = [urlparse.urljoin('http://www.nation.co.ke/', relative_url.strip())]

        #retrieve the author. There are numerous formatting issue with this tag

        author = hxs.xpath('//section[@class=\"author\"]/strong/text()').extract()
        if not author:
            author = hxs.xpath('//section[@class=\"author\"]/text()').extract()
        item["author"] = ''.join(author)

        item["category"] = self.category
        return item

    def scrapStandardOnline(self):
        body = Selector(self.response)

        item = HabariItem()

        item["uri"] = self.response.url
        published = ''.join(body.xpath('//span[@class="updated"]/text()').re(r'[A-Z]\w* \d*[a-z]{2} \d{4}'))
        published = published.replace(u'\xa0', u' ')
        item["published"] = published
        utc = time.strptime(published, "%A, %B %d, %Y");
        timeStamp = time.strptime(utc, " %A, %B %d, %Y");
        item["UTC"] = time.mktime(timeStamp)
        item['published'] = ''.join('%s-%s-%s' % (timeStamp.tm_year, timeStamp.tm_mon, timeStamp.tm_mday))
        item["title"] = ''.join(body.xpath('//div[@class="article-top"]/h1/text()').extract())
        item["summary"] = ''.join(body.xpath('//div[@class="story"]/p[1]/./text()').extract())
        item["content"] = '\n'.join(body.xpath('//div[@class="story"]/p/./text()').extract())
        item["caption"] = ''.join(body.xpath('//table/tbody/tr/td/strong/text()').extract())
        item["tag"] = self.tags + body.xpath('//div[@class="topics"]/a/text()').extract()
        item["author"] = ''  # author from standard is a hit and miss, will need re-tuning
        item["publisher"] = 'Standard'

        # retrieve an image, some images have a relative url
        relative_url = ''.join(body.xpath('//table/tbody/tr/td/img/@src').extract())
        item["image_urls"] = [urlparse.urljoin('http://www.standardmedia.co.ke', relative_url.strip())]

        item["category"] = self.category

        return item



class WorldNewsSpider(CrawlSpider):
    """Scrapes standard News"""
    name = "standard"
    allowed_domains = ["www.standardmedia.co.ke"]
    start_urls = ["http://www.standardmedia.co.ke/business/category/19/business-news"]
    rules = (
        # specific for golem.de -- remove for other sites
        # Rule(SgmlLinkExtractor(allow=('news\/',)), callback='parse_page', follow=True),
        Rule(SgmlLinkExtractor(allow=('business\/article\/',), deny=('article=')), callback='parse_page', follow=True),
    )

    def parse_page(self, response):
        sel = Selector(response)
        mainParser = MainParser(response, ["Business, standardmedia"], 2)
        item = mainParser.scrapStandardOnline()
        return item


class StandardSpider(CrawlSpider):
    """Scrapes standard News"""
    name = "thecounties"
    allowed_domains = ["www.standardmedia.co.ke"]
    start_urls = ["http://www.standardmedia.co.ke/thecounties/"]
    rules = (
        Rule(SgmlLinkExtractor(allow=('thecounties\/article\/',),
                               deny=('article=', 'thecounties\/article\/[0-9]*\/%22+document.location.href+%22')),
             callback='parse_page', follow=True),
    )

    def parse_page(self, response):
        sel = Selector(response)
        mainParser = MainParser(response, ["Counties, standardmedia"], 5)
        item = mainParser.scrapStandardOnline()
        return item


class PoliticsSpider(CrawlSpider):
    """Scrapes Politics News"""
    name = "politics"
    allowed_domains = ["nation.co.ke"]
    start_urls = ["http://www.nation.co.ke/news/politics/-/1064/1064/-/3hxffhz/-/index.html"]
    rules = (
        Rule(SgmlLinkExtractor(allow=('news\/politics\/',),
                               deny=('\/view\/asFeed\/', '\/-\/1064\/1064\/-\/3hxffhz\/-\/index.html')),
             callback='parse_page', follow=True),
    )

    def parse_page(self, response):
        mainParser = MainParser(response, ["News", "Politics"], 1)
        item = mainParser.scrapNationOnline()
        return item

class BusinessNewsSpider(CrawlSpider):
    """Scrapes World News"""
    name = "business"
    allowed_domains = ["nation.co.ke"]
    start_urls = ["http://www.nation.co.ke/business"]
    rules = (
        # Rule(SgmlLinkExtractor(allow=('news\/',)), callback='parse_page', follow=True),
        Rule(SgmlLinkExtractor(allow=('business\/',), deny=('\/view\/asFeed\/')), callback='parse_page', follow=True),
    )

    def parse_page(self, response):
        mainParser = MainParser(response, ["Business", "Corporates", "Enterprise", "Markets"], 2)
        item = mainParser.scrapNationOnline()
        return item


class PopularSpider(CrawlSpider):
    name = "techSpider"
    allowed_domains = ["nation.co.ke"]
    start_urls = ["http://www.nation.co.ke/business/Tech/-/1017288/1017288/-/g8v3dnz/-/index.html"]
    rules = (
        Rule(SgmlLinkExtractor(allow=('business\/Tech\/',), deny=('\/view\/asFeed\/')), callback='parse_page',
             follow=True),
    )

    def parse_page(self, response):
        mainParser = MainParser(response, ["Business", "Technology"], 3)
        item = mainParser.scrapNationOnline()
        return item

class SportsSpider(CrawlSpider):
    name = "sports"
    allowed_domains = ["nation.co.ke"]
    start_urls = ["http://www.nation.co.ke/sports"]
    rules = (
        Rule(SgmlLinkExtractor(allow=('sports\/',), deny=('\/view\/asFeed\/')), callback='parse_page',
             follow=True),
    )

    def parse_page(self, response):
        mainParser = MainParser(response, ["Sports"], 4)
        item = mainParser.scrapNationOnline()
        return item
