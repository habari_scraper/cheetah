#!/usr/bin/python
__author__ = 'edwin-b'

'''
    This is a simple program that does a few things
    1. Queries a scrapyd server hosted on an Amazon EC2 instance,
    2. retrieves a list spiders available,
    3. saves the results in a mongoDB instance hosted on compose.io formerly mongoHQ
'''

import json
import urllib2, urllib
import datetime
from pymongo import MongoClient



URL = 'http://54.91.71.32:6800/listspiders.json?project=habari'
scheduleURL = 'http://54.91.71.32:6800/schedule.json'

request = urllib2.Request(URL)
response = urllib2.urlopen(request)
data = json.load(response)


def logSpider(spiderName, status):

# Open database connection and retrieve the logs collection. Insert spider and server response

    client = MongoClient('mongodb://admin:pass@kahana.mongohq.com/habari', 10084)
    db = client['habari']
    collection = db['logs']

    logs = dict(results=status, spider=spiderName, date=datetime.datetime.utcnow())
    logID = collection.insert(logs)

    print(logID)

for spider in data['spiders']:

    print 'Crawling ' + spider

    values = {'project' : 'habari', 'spider' : spider}
    params = urllib.urlencode(values)
    req = urllib2.Request(scheduleURL, params)
    response = urllib2.urlopen(req)
    status = json.load(response)
    logSpider(spider, status)

